import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/flowbite/**/*.js",
  ],
  safelist: [
    "bg-theme-1",
    "bg-theme-2",
    "bg-theme-3",
    "bg-theme-4",
    "bg-theme-5",
    "bg-theme-6",
    "bg-theme-7",
    "bg-partie-sd",
    "bg-partie-epp",
    "bg-partie-renew",
    "bg-partie-ecr",
    "bg-partie-efa",
    "bg-partie-left",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic": "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        eurhope: {
          100: "#0065F7",
          200: "#FFC81A",
          300: "#C2DBFF",
        },
        theme: {
          1: "#FFFBA2",
          2: "#DBEFD4",
          3: "#FFE2E2",
          4: "#D1F1FB",
          5: "#DDD3EA",
          6: "#FAE7CB",
          7: "#D4CBFA",
        },
        partie: {
          sd: "#D5153C",
          epp: "#FFFFFF",
          renew: "#019EE4",
          ecr: "#FFFFFF",
          efa: "#FFFFFF",
          left: "#FFFFFF",
        },
      },
      fontFamily: {
        circular_light: "Circular-Light",
        circular_medium: "Circular-Medium",
        circular_bold: "Circular-Bold",
        circular_black: "Circular-Black",
        circular_book: "Circular-Book",
        trade_gothic: "Trade Gothic LT Pro",
      },
    },
  },
  daisyui: {
    themes: ["light"],
  },
  plugins: [require("daisyui")],
};
export default config;
