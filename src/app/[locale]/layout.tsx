// app/[locale]/layout.tsx

import "../globals.css";
import type { Metadata } from "next";
import { ReactNode } from "react";
import { notFound } from "next/navigation";
import { NextIntlClientProvider } from "next-intl";
import { unstable_setRequestLocale } from "next-intl/server";

type Props = {
  children: ReactNode;
  params: { locale: string };
};

export const metadata: Metadata = {
  title: "Eurhope",
  description: "Eurhope",
};

//function to get the translations
async function getMessages(locale: string) {
  try {
    return (await import(`../../../i18n/${locale}.json`)).default;
  } catch (error) {
    notFound();
  }
}

//function to generate the routes for all the locales
export function generateStaticParams() {
  return [
    "ro",
    "de",
    "nl",
    "fr",
    "sk",
    "sl",
    "et",
    "bg",
    "fi",
    "pt",
    "hu",
    "cs",
    "el",
    "pl",
    "lt",
    "sv",
    "it",
    "es",
    "lv",
    "da",
    "hr",
    "en",
  ].map((locale) => ({ locale }));
}

export default async function RootLayout({ children, params: { locale } }: Props) {
  unstable_setRequestLocale(locale);

  const messages = await getMessages(locale);

  return (
    <html lang={locale}>
      <body>
        <NextIntlClientProvider locale={locale} messages={messages}>
          {children}
        </NextIntlClientProvider>
      </body>
    </html>
  );
}
