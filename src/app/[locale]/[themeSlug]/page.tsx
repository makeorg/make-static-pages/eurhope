import { unstable_setRequestLocale } from "next-intl/server";
import { useTranslations } from "next-intl";
import AboutSection from "@/components/aboutSection";
import Header from "@/components/header";
import { themes } from "@/data/themes";
import { ideas } from "@/data/ideas";
import ThemeHeader from "@/components/themeHeader";
import MainIdeas from "@/components/mainIdeas";
import OtherDimensionsSection from "@/components/otherDimensionsSection";
import { locales } from "@/navigation";

export function generateStaticParams() {
  return locales.flatMap((locale) =>
    themes.map((theme) => ({ locale, themeSlug: theme.slug }))
  );
}

export default function Theme({ params: { locale, themeSlug } }: any) {
  unstable_setRequestLocale(locale);
  const t = useTranslations();
  const theme = themes.find((theme) => theme.slug === themeSlug);
  const themeIdeas = ideas[themeSlug];

  return (
    <div className="w-full sm:w-full xl:w-full justify-center items-center">
      <Header locale={locale} />
      <ThemeHeader theme={theme} t={t} locale={locale} />
      <MainIdeas theme={theme} ideas={themeIdeas} t={t} locale={locale} />
      <OtherDimensionsSection
        otherThemes={themes
          .filter((otherTheme) => otherTheme.slug !== theme?.slug)
          .slice(0, 3)}
        t={t}
        locale={locale}
      />
      <AboutSection t={t} locale={locale}>
        <div className="flex-col space-y-4">
          <p className="block   text-[20px] leading-[25.3px] font-circular_medium  text-black">
            {t("description_eurhope_1")}
            <span
              dangerouslySetInnerHTML={{
                __html: t.raw("description_eurhope_2"),
              }}
            />
          </p>
          <p
            className="block text-[20px] leading-[25.3px] font-circular_medium  text-black"
            dangerouslySetInnerHTML={{
              __html: t.raw("description_eurhope_3"),
            }}
          />
          {}
        </div>
      </AboutSection>
    </div>
  );
}
