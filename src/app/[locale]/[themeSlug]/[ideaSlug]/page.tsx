import { unstable_setRequestLocale } from "next-intl/server";
import { useTranslations } from "next-intl";
import { themes } from "@/data/themes";
import { ideas } from "@/data/ideas";
import AboutSection from "@/components/aboutSection";
import Header from "@/components/header";
import OtherIdeasSection from "@/components/otherIdeasSection";
import ResponseHeader from "@/components/responseHeader";
import ResponseProposals from "@/components/responseProposals";
import { locales } from "@/navigation";

export function generateStaticParams() {
  return locales.flatMap((locale) =>
    themes.flatMap((theme) =>
      ideas[theme.slug].map((idea: any) => ({
        locale,
        themeSlug: theme.slug,
        ideaSlug: idea.slug,
      }))
    )
  );
}

export default function Response({ params: { locale, themeSlug, ideaSlug } }: any) {
  unstable_setRequestLocale(locale);
  const t = useTranslations();
  const theme = themes.find((theme) => theme.slug === themeSlug);
  const idea = ideas[theme.slug].find((themeIdea: any) => themeIdea.slug === ideaSlug);

  const otherThemeIdeas = themes
    .filter((theme) => theme.slug !== themeSlug)
    .slice(0, 3)
    .map((theme) => {
      const idea = ideas[theme.slug][Math.floor(Math.random() * ideas[theme.slug].length)];

      return {
        slug: idea.slug,
        description: idea.description,
        themeSlug: theme.slug,
        themeTitle: theme.title,
        themeBgColor: theme.bgColor,
      };
    });

  return (
    <div className="w-full justify-center items-center">
      <Header locale={locale} />
      <ResponseHeader theme={theme} idea={idea} t={t} locale={locale} />
      <ResponseProposals theme={theme} idea={idea} suppressHydrationWarning={true} />
      <OtherIdeasSection otherThemeIdeas={otherThemeIdeas} t={t} locale={locale} />
      <AboutSection t={t} locale={locale}>
        <div className="flex-col space-y-4">
          <p className="block   text-[20px] leading-[25.3px] font-circular_medium  text-black">
            {t("description_eurhope_1")}
            <span
              dangerouslySetInnerHTML={{
                __html: t.raw("description_eurhope_2"),
              }}
            />
          </p>
          <p
            className="block text-[20px] leading-[25.3px] font-circular_medium  text-black"
            dangerouslySetInnerHTML={{
              __html: t.raw("description_eurhope_3"),
            }}
          />
        </div>
      </AboutSection>
    </div>
  );
}
