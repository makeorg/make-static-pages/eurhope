import { unstable_setRequestLocale } from "next-intl/server";
import { useTranslations } from "next-intl";
import Header from "@/components/header";
import DescriptionSection from "@/components/firstrool/descriptionSection";
import ThemeSection from "@/components/themesSection";
import AboutSection from "@/components/aboutSection";
import Footer from "@/components/footer";

export default function Home({ params: { locale } }: any) {
  unstable_setRequestLocale(locale);
  const t = useTranslations();

  return (
    <div className="w-full justify-center items-center">
      <Header locale={locale} />
      <DescriptionSection t={t} />
      <ThemeSection t={t} locale={locale} />
      <AboutSection t={t} locale={locale}>
        <p className="block mb-8  leading-[25.3px] font-circular_light text-[20px]  text-black">
          <span
            dangerouslySetInnerHTML={{
              __html: t.raw("welcom_eurhope"),
            }}
          />
          <span
            dangerouslySetInnerHTML={{
              __html: t.raw("about_eurhope_2"),
            }}
          />
          <span
            dangerouslySetInnerHTML={{
              __html: t.raw("about_eurhope_3"),
            }}
          />
        </p>
      </AboutSection>
      <Footer />
    </div>
  );
}
