import alde from "@/assets/parties/alde.png";
import ecr from "@/assets/parties/ecr.png";
import edp from "@/assets/parties/edp.png";
import efa from "@/assets/parties/efa.png";
import eg from "@/assets/parties/eg.png";
import el from "@/assets/parties/el.png";
import epp from "@/assets/parties/epp.png";
import id from "@/assets/parties/id.png";
import pes from "@/assets/parties/pes.png";

export const parties = {
  alde: {
    bgColor: "bg-partie-alde",
    image: alde,
    link: "https://www.aldeparty.eu/",
    hasResponse: true,
  },
  ecr: {
    bgColor: "bg-partie-ecr",
    image: ecr,
    link: "https://ecrparty.eu/",
    hasResponse: false,
  },
  edp: {
    bgColor: "bg-partie-edp",
    image: edp,
    link: "https://democrats.eu/en/",
    hasResponse: true,
  },
  efa: {
    bgColor: "bg-partie-efa",
    image: efa,
    link: "https://e-f-a.org/",
    hasResponse: true,
  },
  greens: {
    bgColor: "bg-partie-greens",
    image: eg,
    link: "https://europeangreens.eu/",
    hasResponse: true,
  },
  left: {
    bgColor: "bg-partie-left",
    image: el,
    link: "https://www.european-left.org/",
    hasResponse: true,
  },
  epp: {
    bgColor: "bg-partie-epp",
    image: epp,
    link: "https://www.epp.eu/",
    hasResponse: false,
  },
  id: {
    bgColor: "bg-partie-id",
    image: id,
    link: "https://id-party.eu/",
    hasResponse: false,
  },
  pes: {
    bgColor: "bg-partie-pes",
    image: pes,
    link: "https://pes.eu/",
    hasResponse: true,
  },
};
