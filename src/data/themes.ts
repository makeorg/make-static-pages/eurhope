import theme1 from "@/assets/theme-1.png";
import theme2 from "@/assets/theme-2.png";
import theme3 from "@/assets/theme-3.png";
import theme4 from "@/assets/theme-4.png";
import theme5 from "@/assets/theme-5.png";
import theme6 from "@/assets/theme-6.png";
import theme7 from "@/assets/theme-7.png";

export const themes: any[] = [
  {
    slug: "democracy-and-eu-institutions",
    bgColor: "bg-theme-1",
    image: theme1,
    title: "Democracy & EU institutions",
  },
  {
    slug: "climate-change-and-environment",
    bgColor: "bg-theme-2",
    image: theme2,
    title: "Climate change & environment",
  },
  {
    slug: "economy-social-justice-and-employment",
    bgColor: "bg-theme-3",
    image: theme3,
    title: "Economy, social justice & employment",
  },
  {
    slug: "eu-in-the-world",
    bgColor: "bg-theme-4",
    image: theme4,
    title: "EU in the world",
  },
  {
    slug: "health-and-education",
    bgColor: "bg-theme-5",
    image: theme5,
    title: "Health & education",
  },
  {
    slug: "human-right-and-migration",
    bgColor: "bg-theme-6",
    image: theme6,
    title: "Human rights & migration",
  },
  {
    slug: "research-and-inovation",
    bgColor: "bg-theme-7",
    image: theme7,
    title: "Research & innovation",
  },
];
