import { parties } from "./parties";
export const ideas: any = {
  "democracy-and-eu-institutions": [
    {
      slug: "idea-1",
      description: "Reinforcing transparency and ethical conduct of elected officials",
      positionBy: "democracy-and-eu-institutions-1-position_by",
      positionText: "democracy-and-eu-institutions-1-position_text",
      linkExtended: "democracy-and-eu-institutions-1-link_extended",
      link: "good-lobby_link",
      link2: "#",
      proposals: [
        {
          author: "democracy-and-eu-institutions-idea-1-proposal-1-author",
          age: 20,
          approvalPercent: 77,
          text: "democracy-and-eu-institutions-idea-1-proposal-1-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-1-proposal-2-author",
          age: 0,
          approvalPercent: 73,
          text: "democracy-and-eu-institutions-idea-1-proposal-2-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-1-proposal-3-author",
          age: 29,
          approvalPercent: 83,
          text: "democracy-and-eu-institutions-idea-1-proposal-3-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-1-proposal-4-author",
          age: 22,
          approvalPercent: 84,
          text: "democracy-and-eu-institutions-idea-1-proposal-4-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-1-proposal-5-author",
          age: 0,
          approvalPercent: 81,
          text: "democracy-and-eu-institutions-idea-1-proposal-5-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-1-proposal-6-author",
          age: 22,
          approvalPercent: 83,
          text: "democracy-and-eu-institutions-idea-1-proposal-6-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-1-proposal-7-author",
          age: 0,
          approvalPercent: 79,
          text: "democracy-and-eu-institutions-idea-1-proposal-7-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-1-proposal-8-author",
          age: 0,
          approvalPercent: 91,
          text: "democracy-and-eu-institutions-idea-1-proposal-8-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "democracy-and-eu-institutions-1-alde-text",
        },
        {
          ...parties.ecr,
          text: "democracy-and-eu-institutions-1-ecr-text",
        },
        {
          ...parties.edp,
          text: "democracy-and-eu-institutions-1-edp-text",
        },
        {
          ...parties.efa,
          text: "democracy-and-eu-institutions-1-efa-text",
        },
        {
          ...parties.greens,
          text: "democracy-and-eu-institutions-1-greens-text",
        },
        {
          ...parties.left,
          text: "democracy-and-eu-institutions-1-left-text",
        },
        {
          ...parties.epp,
          text: "democracy-and-eu-institutions-1-epp-text",
        },
        {
          ...parties.id,
          text: "democracy-and-eu-institutions-1-id-text",
        },
        {
          ...parties.pes,
          text: "democracy-and-eu-institutions-1-pes-text",
        },
      ],
    },
    {
      slug: "idea-2",
      description: "Strengthening the efficiency of European justice systems",
      positionBy: "democracy-and-eu-institutions-2-position_by",
      positionText: "democracy-and-eu-institutions-2-position_text",
      linkExtended: "democracy-and-eu-institutions-2-link_extended",
      link: "#",
      link2: "#",
      proposals: [
        {
          author: "democracy-and-eu-institutions-idea-2-proposal-1-author",
          age: 24,
          approvalPercent: 81,
          text: "democracy-and-eu-institutions-idea-2-proposal-1-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-2-proposal-2-author",
          age: 0,
          approvalPercent: 82,
          text: "democracy-and-eu-institutions-idea-2-proposal-2-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-2-proposal-3-author",
          age: 30,
          approvalPercent: 84,
          text: "democracy-and-eu-institutions-idea-2-proposal-3-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-2-proposal-4-author",
          age: 0,
          approvalPercent: 84,
          text: "democracy-and-eu-institutions-idea-2-proposal-4-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-2-proposal-5-author",
          age: 25,
          approvalPercent: 84,
          text: "democracy-and-eu-institutions-idea-2-proposal-5-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-2-proposal-6-author",
          age: 34,
          approvalPercent: 76,
          text: "democracy-and-eu-institutions-idea-2-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "democracy-and-eu-institutions-2-alde-text",
        },
        {
          ...parties.ecr,
          text: "democracy-and-eu-institutions-2-ecr-text",
        },
        {
          ...parties.edp,
          text: "democracy-and-eu-institutions-2-edp-text",
        },
        {
          ...parties.efa,
          text: "democracy-and-eu-institutions-2-efa-text",
        },
        {
          ...parties.greens,
          text: "democracy-and-eu-institutions-2-greens-text",
        },
        {
          ...parties.left,
          text: "democracy-and-eu-institutions-2-left-text",
        },
        {
          ...parties.epp,
          text: "democracy-and-eu-institutions-2-epp-text",
        },
        {
          ...parties.id,
          text: "democracy-and-eu-institutions-2-id-text",
        },
        {
          ...parties.pes,
          text: "democracy-and-eu-institutions-2-pes-text",
        },
      ],
    },
    {
      slug: "idea-3",
      description: "Increasing citizen participation and their understanding of the EU",
      positionBy: "democracy-and-eu-institutions-3-position_by",
      positionText: "democracy-and-eu-institutions-3-position_text",
      linkExtended: "democracy-and-eu-institutions-3-link_extended",
      link: "democracy-international_link",
      link2: "#",
      proposals: [
        {
          author: "democracy-and-eu-institutions-idea-3-proposal-1-author",
          age: 34,
          approvalPercent: 75,
          text: "democracy-and-eu-institutions-idea-3-proposal-1-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-3-proposal-2-author",
          age: 0,
          approvalPercent: 82,
          text: "democracy-and-eu-institutions-idea-3-proposal-2-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-3-proposal-3-author",
          age: 31,
          approvalPercent: 88,
          text: "democracy-and-eu-institutions-idea-3-proposal-3-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-3-proposal-4-author",
          age: 19,
          approvalPercent: 83,
          text: "democracy-and-eu-institutions-idea-3-proposal-4-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-3-proposal-5-author",
          age: 29,
          approvalPercent: 87,
          text: "democracy-and-eu-institutions-idea-3-proposal-5-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-3-proposal-6-author",
          age: 0,
          approvalPercent: 75,
          text: "democracy-and-eu-institutions-idea-3-proposal-6-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-3-proposal-7-author",
          age: 22,
          approvalPercent: 86,
          text: "democracy-and-eu-institutions-idea-3-proposal-7-text",
        },
        {
          author: "democracy-and-eu-institutions-idea-3-proposal-8-author",
          age: 22,
          approvalPercent: 75,
          text: "democracy-and-eu-institutions-idea-3-proposal-8-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "democracy-and-eu-institutions-3-alde-text",
        },
        {
          ...parties.ecr,
          text: "democracy-and-eu-institutions-3-ecr-text",
        },
        {
          ...parties.edp,
          text: "democracy-and-eu-institutions-3-edp-text",
        },
        {
          ...parties.efa,
          text: "democracy-and-eu-institutions-3-efa-text",
        },
        {
          ...parties.greens,
          text: "democracy-and-eu-institutions-3-greens-text",
        },
        {
          ...parties.left,
          text: "democracy-and-eu-institutions-3-left-text",
        },
        {
          ...parties.epp,
          text: "democracy-and-eu-institutions-3-epp-text",
        },
        {
          ...parties.id,
          text: "democracy-and-eu-institutions-3-id-text",
        },
        {
          ...parties.pes,
          text: "democracy-and-eu-institutions-3-pes-text",
        },
      ],
    },
  ],
  "climate-change-and-environment": [
    {
      slug: "idea-1",
      description: "Developing more efficient and accessible rail transport",
      positionBy: "climate-change-and-environment-1-position_by",
      positionText: "climate-change-and-environment-1-position_text",
      linkExtended: "climate-change-and-environment-1-link_extended",
      link: "gceurope_link",
      link2: "#",
      proposals: [
        {
          author: "climate-change-and-environment-idea-1-proposal-1-author",
          age: 0,
          approvalPercent: 82,
          text: "climate-change-and-environment-idea-1-proposal-1-text",
        },
        {
          author: "climate-change-and-environment-idea-1-proposal-2-author",
          age: 23,
          approvalPercent: 83,
          text: "climate-change-and-environment-idea-1-proposal-2-text",
        },
        {
          author: "climate-change-and-environment-idea-1-proposal-3-author",
          age: 17,
          approvalPercent: 78,
          text: "climate-change-and-environment-idea-1-proposal-3-text",
        },
        {
          author: "climate-change-and-environment-idea-1-proposal-4-author",
          age: 25,
          approvalPercent: 79,
          text: "climate-change-and-environment-idea-1-proposal-4-text",
        },
        {
          author: "climate-change-and-environment-idea-1-proposal-5-author",
          age: 0,
          approvalPercent: 91,
          text: "climate-change-and-environment-idea-1-proposal-5-text",
        },
        {
          author: "climate-change-and-environment-idea-1-proposal-6-author",
          age: 21,
          approvalPercent: 87,
          text: "climate-change-and-environment-idea-1-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "climate-change-and-environment-1-alde-text",
        },
        {
          ...parties.ecr,
          text: "climate-change-and-environment-1-ecr-text",
        },
        {
          ...parties.edp,
          text: "climate-change-and-environment-1-edp-text",
        },
        {
          ...parties.efa,
          text: "climate-change-and-environment-1-efa-text",
        },
        {
          ...parties.greens,
          text: "climate-change-and-environment-1-greens-text",
        },
        {
          ...parties.left,
          text: "climate-change-and-environment-1-left-text",
        },
        {
          ...parties.epp,
          text: "climate-change-and-environment-1-epp-text",
        },
        {
          ...parties.id,
          text: "climate-change-and-environment-1-id-text",
        },
        {
          ...parties.pes,
          text: "climate-change-and-environment-1-pes-text",
        },
      ],
    },
    {
      slug: "idea-2",
      description: "Strengthening sustainable European industries and recycling",
      positionBy: "climate-change-and-environment-2-position_by",
      positionText: "climate-change-and-environment-2-position_text",
      linkExtended: "climate-change-and-environment-2-link_extended",
      link: "gceurope_link",
      link2: "#",
      proposals: [
        {
          author: "climate-change-and-environment-idea-2-proposal-1-author",
          age: 0,
          approvalPercent: 74,
          text: "climate-change-and-environment-idea-2-proposal-1-text",
        },
        {
          author: "climate-change-and-environment-idea-2-proposal-2-author",
          age: 22,
          approvalPercent: 73,
          text: "climate-change-and-environment-idea-2-proposal-2-text",
        },
        {
          author: "climate-change-and-environment-idea-2-proposal-3-author",
          age: 0,
          approvalPercent: 80,
          text: "climate-change-and-environment-idea-2-proposal-3-text",
        },
        {
          author: "climate-change-and-environment-idea-2-proposal-4-author",
          age: 21,
          approvalPercent: 80,
          text: "climate-change-and-environment-idea-2-proposal-4-text",
        },
        {
          author: "climate-change-and-environment-idea-2-proposal-5-author",
          age: 22,
          approvalPercent: 75,
          text: "climate-change-and-environment-idea-2-proposal-5-text",
        },
        {
          author: "climate-change-and-environment-idea-2-proposal-6-author",
          age: 32,
          approvalPercent: 76,
          text: "climate-change-and-environment-idea-2-proposal-6-text",
        },
        {
          author: "climate-change-and-environment-idea-2-proposal-7-author",
          age: 0,
          approvalPercent: 76,
          text: "climate-change-and-environment-idea-2-proposal-7-text",
        },
        {
          author: "climate-change-and-environment-idea-2-proposal-8-author",
          age: 29,
          approvalPercent: 82,
          text: "climate-change-and-environment-idea-2-proposal-8-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "climate-change-and-environment-2-alde-text",
        },
        {
          ...parties.ecr,
          text: "climate-change-and-environment-2-ecr-text",
        },
        {
          ...parties.edp,
          text: "climate-change-and-environment-2-edp-text",
        },
        {
          ...parties.efa,
          text: "climate-change-and-environment-2-efa-text",
        },
        {
          ...parties.greens,
          text: "climate-change-and-environment-2-greens-text",
        },
        {
          ...parties.left,
          text: "climate-change-and-environment-2-left-text",
        },
        {
          ...parties.epp,
          text: "climate-change-and-environment-2-epp-text",
        },
        {
          ...parties.id,
          text: "climate-change-and-environment-2-id-text",
        },
        {
          ...parties.pes,
          text: "climate-change-and-environment-2-pes-text",
        },
      ],
    },
    {
      slug: "idea-3",
      description: "Promoting innovation in European agriculture and sustainable farming",
      positionBy: "climate-change-and-environment-3-position_by",
      positionText: "climate-change-and-environment-3-position_text",
      linkExtended: "climate-change-and-environment-3-link_extended",
      link: "gceurope_link",
      link2: "#",
      proposals: [
        {
          author: "climate-change-and-environment-idea-3-proposal-1-author",
          age: 18,
          approvalPercent: 82,
          text: "climate-change-and-environment-idea-3-proposal-1-text",
        },
        {
          author: "climate-change-and-environment-idea-3-proposal-2-author",
          age: 0,
          approvalPercent: 79,
          text: "climate-change-and-environment-idea-3-proposal-2-text",
        },
        {
          author: "climate-change-and-environment-idea-3-proposal-3-author",
          age: 0,
          approvalPercent: 82,
          text: "climate-change-and-environment-idea-3-proposal-3-text",
        },
        {
          author: "climate-change-and-environment-idea-3-proposal-4-author",
          age: 20,
          approvalPercent: 74,
          text: "climate-change-and-environment-idea-3-proposal-4-text",
        },
        {
          author: "climate-change-and-environment-idea-3-proposal-5-author",
          age: 0,
          approvalPercent: 75,
          text: "climate-change-and-environment-idea-3-proposal-5-text",
        },
        {
          author: "climate-change-and-environment-idea-3-proposal-6-author",
          age: 0,
          approvalPercent: 81,
          text: "climate-change-and-environment-idea-3-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "climate-change-and-environment-3-alde-text",
        },
        {
          ...parties.ecr,
          text: "climate-change-and-environment-3-ecr-text",
        },
        {
          ...parties.edp,
          text: "climate-change-and-environment-3-edp-text",
        },
        {
          ...parties.efa,
          text: "climate-change-and-environment-3-efa-text",
        },
        {
          ...parties.greens,
          text: "climate-change-and-environment-3-greens-text",
        },
        {
          ...parties.left,
          text: "climate-change-and-environment-3-left-text",
        },
        {
          ...parties.epp,
          text: "climate-change-and-environment-3-epp-text",
        },
        {
          ...parties.id,
          text: "climate-change-and-environment-3-id-text",
        },
        {
          ...parties.pes,
          text: "climate-change-and-environment-3-pes-text",
        },
      ],
    },
    {
      slug: "idea-4",
      description: "Increasing the protection of biodiversity within the EU and beyond",
      positionBy: "climate-change-and-environment-4-position_by",
      positionText: "climate-change-and-environment-4-position_text",
      linkExtended: "climate-change-and-environment-4-link_extended",
      link: "gceurope_link",
      link2: "#",
      proposals: [
        {
          author: "climate-change-and-environment-idea-4-proposal-1-author",
          age: 25,
          approvalPercent: 87,
          text: "climate-change-and-environment-idea-4-proposal-1-text",
        },
        {
          author: "climate-change-and-environment-idea-4-proposal-2-author",
          age: 19,
          approvalPercent: 80,
          text: "climate-change-and-environment-idea-4-proposal-2-text",
        },
        {
          author: "climate-change-and-environment-idea-4-proposal-3-author",
          age: 23,
          approvalPercent: 72,
          text: "climate-change-and-environment-idea-4-proposal-3-text",
        },
        {
          author: "climate-change-and-environment-idea-4-proposal-4-author",
          age: 26,
          approvalPercent: 86,
          text: "climate-change-and-environment-idea-4-proposal-4-text",
        },
        {
          author: "climate-change-and-environment-idea-4-proposal-5-author",
          age: 34,
          approvalPercent: 94,
          text: "climate-change-and-environment-idea-4-proposal-5-text",
        },
        {
          author: "climate-change-and-environment-idea-4-proposal-6-author",
          age: 34,
          approvalPercent: 78,
          text: "climate-change-and-environment-idea-4-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "climate-change-and-environment-4-alde-text",
        },
        {
          ...parties.ecr,
          text: "climate-change-and-environment-4-ecr-text",
        },
        {
          ...parties.edp,
          text: "climate-change-and-environment-4-edp-text",
        },
        {
          ...parties.efa,
          text: "climate-change-and-environment-4-efa-text",
        },
        {
          ...parties.greens,
          text: "climate-change-and-environment-4-greens-text",
        },
        {
          ...parties.left,
          text: "climate-change-and-environment-4-left-text",
        },
        {
          ...parties.epp,
          text: "climate-change-and-environment-4-epp-text",
        },
        {
          ...parties.id,
          text: "climate-change-and-environment-4-id-text",
        },
        {
          ...parties.pes,
          text: "climate-change-and-environment-4-pes-text",
        },
      ],
    },
    {
      slug: "idea-5",
      description: "Accelerating the energy transition and the reduction of carbon emissions",
      positionBy: "climate-change-and-environment-5-position_by",
      positionText: "climate-change-and-environment-5-position_text",
      linkExtended: "climate-change-and-environment-5-link_extended",
      link: "gceurope_link",
      link2: "#",
      proposals: [
        {
          author: "climate-change-and-environment-idea-5-proposal-1-author",
          age: 29,
          approvalPercent: 66,
          text: "climate-change-and-environment-idea-5-proposal-1-text",
        },
        {
          author: "climate-change-and-environment-idea-5-proposal-2-author",
          age: 22,
          approvalPercent: 85,
          text: "climate-change-and-environment-idea-5-proposal-2-text",
        },
        {
          author: "climate-change-and-environment-idea-5-proposal-3-author",
          age: 18,
          approvalPercent: 67,
          text: "climate-change-and-environment-idea-5-proposal-3-text",
        },
        {
          author: "climate-change-and-environment-idea-5-proposal-4-author",
          age: 23,
          approvalPercent: 78,
          text: "climate-change-and-environment-idea-5-proposal-4-text",
        },
        {
          author: "climate-change-and-environment-idea-5-proposal-5-author",
          age: 20,
          approvalPercent: 78,
          text: "climate-change-and-environment-idea-5-proposal-5-text",
        },
        {
          author: "climate-change-and-environment-idea-5-proposal-6-author",
          age: 34,
          approvalPercent: 78,
          text: "climate-change-and-environment-idea-5-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "climate-change-and-environment-5-alde-text",
        },
        {
          ...parties.ecr,
          text: "climate-change-and-environment-5-ecr-text",
        },
        {
          ...parties.edp,
          text: "climate-change-and-environment-5-edp-text",
        },
        {
          ...parties.efa,
          text: "climate-change-and-environment-5-efa-text",
        },
        {
          ...parties.greens,
          text: "climate-change-and-environment-5-greens-text",
        },
        {
          ...parties.left,
          text: "climate-change-and-environment-5-left-text",
        },
        {
          ...parties.epp,
          text: "climate-change-and-environment-5-epp-text",
        },
        {
          ...parties.id,
          text: "climate-change-and-environment-5-id-text",
        },
        {
          ...parties.pes,
          text: "climate-change-and-environment-5-pes-text",
        },
      ],
    },
  ],
  "economy-social-justice-and-employment": [
    {
      slug: "idea-1",
      description: "Promoting easier access to employment, especially for the youth",
      positionBy: "economy-social-justice-and-employment-1-position_by",
      positionText: "economy-social-justice-and-employment-1-position_text",
      linkExtended: "economy-social-justice-and-employment-1-link_extended",
      link: "sgieurope_link",
      link2: "#",
      proposals: [
        {
          author: "economy-social-justice-and-employment-idea-1-proposal-1-author",
          age: 25,
          approvalPercent: 75,
          text: "economy-social-justice-and-employment-idea-1-proposal-1-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-1-proposal-2-author",
          age: 33,
          approvalPercent: 75,
          text: "economy-social-justice-and-employment-idea-1-proposal-2-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-1-proposal-3-author",
          age: 34,
          approvalPercent: 80,
          text: "economy-social-justice-and-employment-idea-1-proposal-3-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-1-proposal-4-author",
          age: 0,
          approvalPercent: 74,
          text: "economy-social-justice-and-employment-idea-1-proposal-4-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-1-proposal-5-author",
          age: 28,
          approvalPercent: 75,
          text: "economy-social-justice-and-employment-idea-1-proposal-5-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-1-proposal-6-author",
          age: 30,
          approvalPercent: 75,
          text: "economy-social-justice-and-employment-idea-1-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "economy-social-justice-and-employment-1-alde-text",
        },
        {
          ...parties.ecr,
          text: "economy-social-justice-and-employment-1-ecr-text",
        },
        {
          ...parties.edp,
          text: "economy-social-justice-and-employment-1-edp-text",
        },
        {
          ...parties.efa,
          text: "economy-social-justice-and-employment-1-efa-text",
        },
        {
          ...parties.greens,
          text: "economy-social-justice-and-employment-1-greens-text",
        },
        {
          ...parties.left,
          text: "economy-social-justice-and-employment-1-left-text",
        },
        {
          ...parties.epp,
          text: "economy-social-justice-and-employment-1-epp-text",
        },
        {
          ...parties.id,
          text: "economy-social-justice-and-employment-1-id-text",
        },
        {
          ...parties.pes,
          text: "economy-social-justice-and-employment-1-pes-text",
        },
      ],
    },
    {
      slug: "idea-2",
      description: "Encouraging “Made in Europe”",
      positionBy: "economy-social-justice-and-employment-2-position_by",
      positionText: "economy-social-justice-and-employment-2-position_text",
      linkExtended: "economy-social-justice-and-employment-2-link_extended",
      link: "beuc_link",
      link2: "sgieurope_link",
      proposals: [
        {
          author: "economy-social-justice-and-employment-idea-2-proposal-1-author",
          age: 33,
          approvalPercent: 80,
          text: "economy-social-justice-and-employment-idea-2-proposal-1-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-2-proposal-2-author",
          age: 0,
          approvalPercent: 85,
          text: "economy-social-justice-and-employment-idea-2-proposal-2-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-2-proposal-3-author",
          age: 0,
          approvalPercent: 86,
          text: "economy-social-justice-and-employment-idea-2-proposal-3-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-2-proposal-4-author",
          age: 19,
          approvalPercent: 91,
          text: "economy-social-justice-and-employment-idea-2-proposal-4-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-2-proposal-5-author",
          age: 33,
          approvalPercent: 86,
          text: "economy-social-justice-and-employment-idea-2-proposal-5-text",
        },
        {
          author: "economy-social-justice-and-employment-idea-2-proposal-6-author",
          age: 0,
          approvalPercent: 86,
          text: "economy-social-justice-and-employment-idea-2-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "economy-social-justice-and-employment-2-alde-text",
        },
        {
          ...parties.ecr,
          text: "economy-social-justice-and-employment-2-ecr-text",
        },
        {
          ...parties.edp,
          text: "economy-social-justice-and-employment-2-edp-text",
        },
        {
          ...parties.efa,
          text: "economy-social-justice-and-employment-2-efa-text",
        },
        {
          ...parties.greens,
          text: "economy-social-justice-and-employment-2-greens-text",
        },
        {
          ...parties.left,
          text: "economy-social-justice-and-employment-2-left-text",
        },
        {
          ...parties.epp,
          text: "economy-social-justice-and-employment-2-epp-text",
        },
        {
          ...parties.id,
          text: "economy-social-justice-and-employment-2-id-text",
        },
        {
          ...parties.pes,
          text: "economy-social-justice-and-employment-2-pes-text",
        },
      ],
    },
  ],
  "eu-in-the-world": [
    {
      slug: "idea-1",
      description: "Strengthening a sovereign EU voice in diplomacy",
      positionBy: "eu-in-the-world-1-position_by",
      positionText: "eu-in-the-world-1-position_text",
      linkExtended: "eu-in-the-world-1-link_extended",
      link: "#",
      link2: "#",
      proposals: [
        {
          author: "eu-in-the-world-idea-1-proposal-1-author",
          age: 22,
          approvalPercent: 78,
          text: "eu-in-the-world-idea-1-proposal-1-text",
        },
        {
          author: "eu-in-the-world-idea-1-proposal-2-author",
          age: 32,
          approvalPercent: 83,
          text: "eu-in-the-world-idea-1-proposal-2-text",
        },
        {
          author: "eu-in-the-world-idea-1-proposal-3-author",
          age: 0,
          approvalPercent: 75,
          text: "eu-in-the-world-idea-1-proposal-3-text",
        },
        {
          author: "eu-in-the-world-idea-1-proposal-4-author",
          age: 24,
          approvalPercent: 80,
          text: "eu-in-the-world-idea-1-proposal-4-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "eu-in-the-world-1-alde-text",
        },
        {
          ...parties.ecr,
          text: "eu-in-the-world-1-ecr-text",
        },
        {
          ...parties.edp,
          text: "eu-in-the-world-1-edp-text",
        },
        {
          ...parties.efa,
          text: "eu-in-the-world-1-efa-text",
        },
        {
          ...parties.greens,
          text: "eu-in-the-world-1-greens-text",
        },
        {
          ...parties.left,
          text: "eu-in-the-world-1-left-text",
        },
        {
          ...parties.epp,
          text: "eu-in-the-world-1-epp-text",
        },
        {
          ...parties.id,
          text: "eu-in-the-world-1-id-text",
        },
        {
          ...parties.pes,
          text: "eu-in-the-world-1-pes-text",
        },
      ],
    },
  ],
  "health-and-education": [
    {
      slug: "idea-1",
      description: "Enhancing measures at the European level to improve and coordinate education",
      positionBy: "health-and-education-1-position_by",
      positionText: "health-and-education-1-position_text",
      linkExtended: "health-and-education-1-link_extended",
      link: "esn_link",
      link2: "esu_link",
      proposals: [
        {
          author: "health-and-education-idea-1-proposal-1-author",
          age: 26,
          approvalPercent: 76,
          text: "health-and-education-idea-1-proposal-1-text",
        },
        {
          author: "health-and-education-idea-1-proposal-2-author",
          age: 31,
          approvalPercent: 75,
          text: "health-and-education-idea-1-proposal-2-text",
        },
        {
          author: "health-and-education-idea-1-proposal-3-author",
          age: 33,
          approvalPercent: 83,
          text: "health-and-education-idea-1-proposal-3-text",
        },
        {
          author: "health-and-education-idea-1-proposal-4-author",
          age: 20,
          approvalPercent: 86,
          text: "health-and-education-idea-1-proposal-4-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "health-and-education-1-alde-text",
        },
        {
          ...parties.ecr,
          text: "health-and-education-1-ecr-text",
        },
        {
          ...parties.edp,
          text: "health-and-education-1-edp-text",
        },
        {
          ...parties.efa,
          text: "health-and-education-1-efa-text",
        },
        {
          ...parties.greens,
          text: "health-and-education-1-greens-text",
        },
        {
          ...parties.left,
          text: "health-and-education-1-left-text",
        },
        {
          ...parties.epp,
          text: "health-and-education-1-epp-text",
        },
        {
          ...parties.id,
          text: "health-and-education-1-id-text",
        },
        {
          ...parties.pes,
          text: "health-and-education-1-pes-text",
        },
      ],
    },
    {
      slug: "idea-2",
      description: "Strengthening EU action on inclusive and harmonised healthcare systems",
      positionBy: "health-and-education-2-position_by",
      positionText: "health-and-education-2-position_text",
      linkExtended: "health-and-education-2-link_extended",
      link: "#",
      link2: "#",
      proposals: [
        {
          author: "health-and-education-idea-2-proposal-1-author",
          age: 32,
          approvalPercent: 77,
          text: "health-and-education-idea-2-proposal-1-text",
        },
        {
          author: "health-and-education-idea-2-proposal-2-author",
          age: 33,
          approvalPercent: 79,
          text: "health-and-education-idea-2-proposal-2-text",
        },
        {
          author: "health-and-education-idea-2-proposal-3-author",
          age: 0,
          approvalPercent: 87,
          text: "health-and-education-idea-2-proposal-3-text",
        },
        {
          author: "health-and-education-idea-2-proposal-4-author",
          age: 32,
          approvalPercent: 84,
          text: "health-and-education-idea-2-proposal-4-text",
        },
        {
          author: "health-and-education-idea-2-proposal-5-author",
          age: 0,
          approvalPercent: 88,
          text: "health-and-education-idea-2-proposal-5-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "health-and-education-2-alde-text",
        },
        {
          ...parties.ecr,
          text: "health-and-education-2-ecr-text",
        },
        {
          ...parties.edp,
          text: "health-and-education-2-edp-text",
        },
        {
          ...parties.efa,
          text: "health-and-education-2-efa-text",
        },
        {
          ...parties.greens,
          text: "health-and-education-2-greens-text",
        },
        {
          ...parties.left,
          text: "health-and-education-2-left-text",
        },
        {
          ...parties.epp,
          text: "health-and-education-2-epp-text",
        },
        {
          ...parties.id,
          text: "health-and-education-2-id-text",
        },
        {
          ...parties.pes,
          text: "health-and-education-2-pes-text",
        },
      ],
    },
  ],
  "human-right-and-migration": [
    {
      slug: "idea-1",
      description: "Creating a more efficient European legal framework for immigration",
      positionBy: "human-right-and-migration-1-position_by",
      positionText: "human-right-and-migration-1-position_text",
      linkExtended: "human-right-and-migration-1-link_extended",
      link: "voicify_link",
      link2: "#",
      proposals: [
        {
          author: "human-right-and-migration-idea-1-proposal-1-author",
          age: 34,
          approvalPercent: 72,
          text: "human-right-and-migration-idea-1-proposal-1-text",
        },
        {
          author: "human-right-and-migration-idea-1-proposal-2-author",
          age: 17,
          approvalPercent: 72,
          text: "human-right-and-migration-idea-1-proposal-2-text",
        },
        {
          author: "human-right-and-migration-idea-1-proposal-3-author",
          age: 34,
          approvalPercent: 72,
          text: "human-right-and-migration-idea-1-proposal-3-text",
        },
        {
          author: "human-right-and-migration-idea-1-proposal-4-author",
          age: 24,
          approvalPercent: 74,
          text: "human-right-and-migration-idea-1-proposal-4-text",
        },
        {
          author: "human-right-and-migration-idea-1-proposal-5-author",
          age: 18,
          approvalPercent: 80,
          text: "human-right-and-migration-idea-1-proposal-5-text",
        },
        {
          author: "human-right-and-migration-idea-1-proposal-6-author",
          age: 24,
          approvalPercent: 85,
          text: "human-right-and-migration-idea-1-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "human-right-and-migration-1-alde-text",
        },
        {
          ...parties.ecr,
          text: "human-right-and-migration-1-ecr-text",
        },
        {
          ...parties.edp,
          text: "human-right-and-migration-1-edp-text",
        },
        {
          ...parties.efa,
          text: "human-right-and-migration-1-efa-text",
        },
        {
          ...parties.greens,
          text: "human-right-and-migration-1-greens-text",
        },
        {
          ...parties.left,
          text: "human-right-and-migration-1-left-text",
        },
        {
          ...parties.epp,
          text: "human-right-and-migration-1-epp-text",
        },
        {
          ...parties.id,
          text: "human-right-and-migration-1-id-text",
        },
        {
          ...parties.pes,
          text: "human-right-and-migration-1-pes-text",
        },
      ],
    },
  ],
  "research-and-inovation": [
    {
      slug: "idea-1",
      description: "Supporting research, innovation and technology in Europe",
      positionBy: "research-and-inovation-1-position_by",
      positionText: "research-and-inovation-1-position_text",
      linkExtended: "research-and-inovation-1-link_extended",
      link: "sga_link",
      link2: "#",
      proposals: [
        {
          author: "research-and-inovation-idea-1-proposal-1-author",
          age: 30,
          approvalPercent: 76,
          text: "research-and-inovation-idea-1-proposal-1-text",
        },
        {
          author: "research-and-inovation-idea-1-proposal-2-author",
          age: 34,
          approvalPercent: 87,
          text: "research-and-inovation-idea-1-proposal-2-text",
        },
        {
          author: "research-and-inovation-idea-1-proposal-3-author",
          age: 0,
          approvalPercent: 78,
          text: "research-and-inovation-idea-1-proposal-3-text",
        },
        {
          author: "research-and-inovation-idea-1-proposal-4-author",
          age: 22,
          approvalPercent: 85,
          text: "research-and-inovation-idea-1-proposal-4-text",
        },
        {
          author: "research-and-inovation-idea-1-proposal-5-author",
          age: 0,
          approvalPercent: 77,
          text: "research-and-inovation-idea-1-proposal-5-text",
        },
        {
          author: "research-and-inovation-idea-1-proposal-6-author",
          age: 18,
          approvalPercent: 80,
          text: "research-and-inovation-idea-1-proposal-6-text",
        },
      ],
      partiesResponse: [
        {
          ...parties.alde,
          text: "research-and-inovation-1-alde-text",
        },
        {
          ...parties.ecr,
          text: "research-and-inovation-1-ecr-text",
        },
        {
          ...parties.edp,
          text: "research-and-inovation-1-edp-text",
        },
        {
          ...parties.efa,
          text: "research-and-inovation-1-efa-text",
        },
        {
          ...parties.greens,
          text: "research-and-inovation-1-greens-text",
        },
        {
          ...parties.left,
          text: "research-and-inovation-1-left-text",
        },
        {
          ...parties.epp,
          text: "research-and-inovation-1-epp-text",
        },
        {
          ...parties.id,
          text: "research-and-inovation-1-id-text",
        },
        {
          ...parties.pes,
          text: "research-and-inovation-1-pes-text",
        },
      ],
    },
  ],
};
