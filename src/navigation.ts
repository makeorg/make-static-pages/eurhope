import { createSharedPathnamesNavigation } from "next-intl/navigation";

export const locales = [
  "bg",
  "es",
  "cs",
  "da",
  "de",
  "et",
  "el",
  "en",
  "fr",
  "hr",
  "it",
  "lv",
  "lt",
  "hu",
  "nl",
  "pl",
  "pt",
  "ro",
  "sk",
  "sl",
  "fi",
  "sv",
] as const;

export const localesName = {
  bg: "Bulgarian",
  es: "Spanish",
  ro: "Romanian",
  de: "Deutsch",
  nl: "Dutch",
  fr: "French",
  sk: "Slovak",
  sl: "Slovenian",
  et: "Estonian",
  fi: "Finnish",
  pt: "Portuguese",
  hu: "Hungarian",
  cs: "Czech",
  el: "Greek",
  pl: "Polish",
  lt: "Lithuanian",
  sv: "Swedish",
  it: "Italian",
  lv: "Latvian",
  da: "Danish",
  hr: "Croatian",
  en: "English",
};
export const localePrefix = "always"; // Default

export const { Link, redirect, usePathname, useRouter } = createSharedPathnamesNavigation({ locales, localePrefix });
