import { notFound } from "next/navigation";
import { getRequestConfig } from "next-intl/server";

// Can be imported from a shared config
const locales = [
  "ro",
  "de",
  "nl",
  "fr",
  "sk",
  "sl",
  "et",
  "bg",
  "fi",
  "pt",
  "hu",
  "cs",
  "el",
  "pl",
  "lt",
  "sv",
  "it",
  "es",
  "lv",
  "da",
  "hr",
  "en",
];

export default getRequestConfig(async ({ locale }) => {
  // Validate that the incoming `locale` parameter is valid
  if (!locales.includes(locale as any)) notFound();

  return {
    messages: (await import(`../i18n/${locale}.json`)).default,
  };
});
