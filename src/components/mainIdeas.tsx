import Link from "next/link";

const MainIdeas = ({ theme, ideas, t, locale }: any) => {
  return (
    <div className="w-full sm:w-full min-h-[520px] bg-[#F7F7F7] py-5 flex space-y-6  flex-col justify-center items-center">
      <div className="flex-row space-y-4">
        <p className="flex justify-center font-circular_bold  text-[42px]  tracking-wide  ">
          {t("main_idea_1")}
          <span className="bg-eurhope-200 px-1 ml-2 ">{t("main_idea_2")}</span>
        </p>
        <div className=" px-10 flex justify-center items-center ">
          <p className="font-circular_bold text-center text-[18px] text-black ">
            {t("These ideas gathered over 65% votes in favour")}
          </p>
        </div>
      </div>

      <div className="grid gap-x-[30px] gap-y-[30px] md:grid-cols-2  lg:grid-cols-3 xl:grid-cols-3 p-8">
        {ideas.map((idea: any) => (
          <div
            key={`${theme.slug}-${ideas.slug}`}
            className=" bg-white  w-full max-w-[360px]  rounded shadow-lg  p-[30px] "
          >
            <div className="flex-col items-start w-full  space-y-10">
              <div className="flex-col items-start space-y-5">
                <span className={`text-black ${theme.bgColor} px-3 py-1 font-trade_gothic text-[16px] uppercase`}>
                  {t(theme.title)}
                </span>
                <p className="text-[16px] font-circular_medium text-black">{t(idea.description)}</p>
              </div>
              <div className=" flex-col items-start justify-end">
                <Link
                  href={`/${locale}/${theme.slug}/${idea.slug}`}
                  className="text-[#D5153C] font-circular_medium underline text-[16px] "
                >
                  {t("Discover this idea")}
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default MainIdeas;
