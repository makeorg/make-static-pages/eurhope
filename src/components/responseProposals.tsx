"use client";
import { useTranslations } from "next-intl";
import { usePathname, useRouter } from "../navigation";
import CustomCarousel from "./carousel";
import ResponsesSecondRool from "./secondrool/responsesSecondRool";

const ResponseProposals = ({ idea }: any) => {
  const t = useTranslations();

  const router = useRouter();
  const pathname = usePathname();

  return (
    <>
      <div className="w-full sm:w-full min-h-[976px] bg-[#F7F7F7]  space-y-12 flex py-5  flex-col justify-center items-center">
        <div className="flex-row  justify-center items-center space-y-10">
          <p className="flex  justify-center items-center font-circular_bold text-[25px] sm:text-[42px] leading-[63px] tracking-wide  ">
            {t("proposal_by")}
            <span className="bg-eurhope-200  ml-2 px-2 ">{t("young_citizens")}</span>
          </p>
          <p className="font-circular_bold text-[18px] text-center text-black ">
            {t("Find a selection of the proposals made by young Europeans")}
          </p>
        </div>
        <CustomCarousel idea={idea} />

        <div className="lg:w-[800px] w-full px-14 lg:px-8 py-2 bg-[#00C058] bg-opacity-[10%] my-5 shadow-lg flex text-center flex-col items-center justify-center">
          <div className="items-center md:text-10 px-10 py-2 bg-[#00C058] font-circular_bold text-center text-white uppercase rounded-sm">
            {t.rich(idea.positionBy, {
              link: (chunks) => (
                <a className="underline" target="_blank" href={t(idea.link)}>
                  {chunks}
                </a>
              ),
              link2: (chunks) => (
                <a className="underline" target="_blank" href={t(idea.link2)}>
                  {chunks}
                </a>
              ),
            })}
          </div>
          <p className="w-full font-circular_book mt-2 mb-2 lg:text-md  leading-[25px]">
            {t.rich(idea.positionText, {
              br: () => <br />,
              "link-extended": (chunks) => (
                <a className="underline" target="_blank" href={t(idea.linkExtended)}>
                  {chunks}
                </a>
              ),
            })}
          </p>
        </div>
      </div>
      <ResponsesSecondRool responses={idea.partiesResponse} t={t} suppressHydrationWarning={true} />

      <div className="w-full  flex items-center justify-center px-10  py-10 lg:h-20">
        <p className="font-circular_medium text-center   text-[16px]">
          {t("answer_eurhope_1")}
          <span className="cursor-pointer" onClick={() => router.push(pathname, { locale: "en" })}>
            {t("answer_eurhope_2")}
          </span>
        </p>
      </div>
      {/*<div className="w-full  flex items-center justify-center px-10  py-10 lg:h-20">
        <p className="font-circular_medium text-center   text-[16px]">{t("candidate_response")}</p>
      </div>*/}
    </>
  );
};

export default ResponseProposals;
