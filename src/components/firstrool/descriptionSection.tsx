import Image from "next/image";
import eurHope from "@/assets/euro.png";
import make from "@/assets/make.svg";
import yef from "@/assets/yef.svg";
import european_parl from "@/assets/european-parl.png";
const DescriptionSection = ({ t }: any) => {
  return (
    <div className=" flex-col w-full  bg-white">
      <div className=" top-0 flex justify-center items-center w-full md:w-full h-[557px] bg-eurhope-100 ">
        <div className="flex-col justify-center items-center  px-10">
          <Image src={eurHope} width={694} height={390} alt="Picture of EurHope" />
          <div className="flex justify-center items-end space-x-10 space-y-10">
            <Image src={make} alt="make logo" />
            <Image src={yef} alt="yef logo" />
            <div>
              <span className="text-white font-circular_light">Supported by</span>
              <Image src={european_parl} alt="europeal parliament logo" />
            </div>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-center py-20">
        <div className="flex-col items-center justify-center px-5 lg:px-40 space-y-10">
          <p className="text-center font-circular_bold text-black text-[40px] tracking-wide">
            {t("Explore the")}
            <span className="bg-eurhope-200 px-1 py-1 ml-2 ">{t("agenda of hope")}</span>
            <br />
            {t.rich("european-political-parties", {
              response: (chunks: any) => <span className="bg-eurhope-200 px-1 py-1 ml-2 "> {chunks}</span>,
            })}
          </p>
          <p className=" w-full text-center font-circular_light text-black text-[20px] lg:px-28 ">
            <span
              dangerouslySetInnerHTML={{
                __html: t.raw("welcom_eurhope"),
              }}
            />
            <span
              dangerouslySetInnerHTML={{
                __html: t.raw("welcom_eurhope_2"),
              }}
            />
            <span
              dangerouslySetInnerHTML={{
                __html: t.raw("welcom_eurhope_3"),
              }}
            />
          </p>
        </div>
      </div>
    </div>
  );
};

export default DescriptionSection;
