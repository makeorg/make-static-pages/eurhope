import Image from "next/image";
import Link from "next/link";

export const OtherDimensionsSection = ({ otherThemes, t, locale }: any) => {
  return (
    <div className="w-full min-h-[628px] bg-black flex space-y-10  bg-opacity-5 flex-col justify-center items-center px-10 py-10">
      <div className="flex justify-center items-center text-center">
        <p className=" font-circular_bold text-center text-[42px]">{t("Other dimensions that might interest you")}</p>
      </div>
      <div className="grid gap-x-8 gap-y-8 md:grid-cols-3 sm:grid-cols-2 lg:grid-cols-3 ">
        {otherThemes.map((theme: any) => (
          <div
            key={theme.slug}
            className={`${theme.bgColor} bg-opacity-[20%] w-full max-h-[350px] max-w-[360px] rounded shadow-lg  p-[30px]`}
          >
            <div className="flex-row items-start space-y-5">
              <div className="flex-row items-start space-y-5">
                <Image
                  width={400}
                  src={theme.image}
                  alt={theme.title}
                  className=" object-cover w-[300px] h-[150px]"
                  loading="lazy"
                />
                <div className="w-full h-[25px] ">
                  <span className={`text-black ${theme.bgColor} px-1 py-1 font-trade_gothic text-[16px] uppercase`}>
                    {t(theme.title)}
                  </span>
                </div>
              </div>
              <div>
                <Link
                  href={`/${locale}/${theme.slug}`}
                  className="text-[#D5153C] font-circular_medium underline text-[16px] "
                >
                  {t("Explore this dimension")}
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
      <Link
        href={`/${locale}`}
        className="min-w-[198px] min-h-[35px] px-2 py-2  bg-[#D5153C] font-trade_gothic text-[16px]  text-center text-white  uppercase rounded-[58px]  "
        type="button"
      >
        {t("BACK TO ALL 7 DIMENSIONS")}
      </Link>
    </div>
  );
};

export default OtherDimensionsSection;
