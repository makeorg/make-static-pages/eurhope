import { themes } from "@/data/themes";
import ThemeCard from "./themeCard";

const ThemesSection = ({ t, locale }: any) => {
  return (
    <div className=" w-full min-h-[1130px] flex justify-center items-center py-[100px] bg-black bg-opacity-[4%]">
      <div className="grid gap-x-8 gap-y-8 md:grid-cols-2  lg:grid-cols-3 xl:grid-cols-3 p-8">
        {themes.map((theme) => {
          return <ThemeCard theme={theme} key={theme.slug} t={t} locale={locale} />;
        })}
      </div>
    </div>
  );
};

export default ThemesSection;
