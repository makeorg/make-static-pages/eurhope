"use client"; // <===== REQUIRED
// @ts-ignore
import accord from "@/assets/accord.svg";
import Image from "next/image";
import { Splide, SplideSlide, SplideTrack } from "@splidejs/react-splide";
import "@splidejs/react-splide/css";
import user from "@/assets/avatar.png";
import { useTranslations } from "next-intl";

const CustomCarousel = ({ idea }: any) => {
  const t = useTranslations();

  return (
    <Splide
      hasTrack={false}
      options={{
        perPage: 3,
        breakpoints: {
          640: {
            perPage: 1,
            pagination: false,
            arrows: false,
          },
          1024: {
            perPage: 2,
            pagination: false,
            arrows: false,
          },
        },
        gap: "1rem",
        pagination: true,
        loop: true,
      }}
      className=" w-[70%] flex items-center justify-center lg:px-20 py-10 "
    >
      <SplideTrack className="py-10">
        {idea.proposals.map((proposal: any) => (
          <SplideSlide
            key={proposal.text}
            className="relative bg-white max-w-[306px] rounded-[8px] flex-row content-between items-center justify-center  shadow-xl "
          >
            <div className=" w-[55px] h-[55px] absolute -mt-10 w-full flex justify-center">
              <Image src={user} alt="user svg" />
            </div>
            <div className="text-center mt-4">
              <span className={`text-gray-500 font-circular_medium leading-[12px] text-[16px] `}>
                {t(proposal.author)}
                {proposal.age > 0 ? `, ${proposal.age}` : ""}
              </span>

              <p className="font-circular_medium p-[10px] text-[16px] ">{t(proposal.text)}</p>
              <div className="flex space-x-2 items-center justify-center ">
                <Image src={accord} alt="accord icon" />
                <span className="font-circular_bold text-[14px] mt-1">
                  {t("approval_text", {
                    percent: proposal.approvalPercent,
                  })}
                </span>
              </div>
            </div>
          </SplideSlide>
        ))}
      </SplideTrack>
      <div className="splide__arrows">
        <button className="splide__arrow splide__arrow--prev">
          <svg width="17" height="13" viewBox="0 0 17 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M7.59086 2.57835L9.93279 5.04482L0.357143 5.04482C-0.116249 5.04482 -0.5 5.44898 -0.5 5.94756L-0.5 6.8503C-0.5 7.34887 -0.116249 7.75303 0.357143 7.75303L9.93279 7.75303L7.59086 10.2195C7.25611 10.5721 7.25611 11.1436 7.59086 11.4962L8.197 12.1345C8.53175 12.4871 9.07447 12.4871 9.40918 12.1345L14.2489 7.03724C14.5837 6.68468 14.5837 6.1131 14.2489 5.76058L9.40918 0.663342C9.07443 0.310786 8.53171 0.310786 8.197 0.663343L7.59086 1.30169C7.25611 1.65421 7.25611 2.22579 7.59086 2.57835Z"
              fill="black"
            />
          </svg>
        </button>
        <button className="splide__arrow splide__arrow--next">
          <svg width="17" height="13" viewBox="0 0 17 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M9.59086 2.57835L11.9328 5.04482L1.35714 5.04482C0.883751 5.04482 0.5 5.44898 0.5 5.94756L0.500001 6.8503C0.500001 7.34887 0.883751 7.75303 1.35714 7.75303L11.9328 7.75303L9.59086 10.2195C9.25611 10.5721 9.25611 11.1436 9.59086 11.4962L10.197 12.1345C10.5317 12.4871 11.0744 12.4871 11.4091 12.1345L16.2489 7.03724C16.5837 6.68468 16.5837 6.1131 16.2489 5.76058L11.4091 0.663342C11.0744 0.310786 10.5317 0.310786 10.197 0.663343L9.59086 1.30169C9.25611 1.65421 9.25611 2.22579 9.59086 2.57835Z"
              fill="black"
            />
          </svg>
        </button>
      </div>
    </Splide>
  );
};
export default CustomCarousel;
