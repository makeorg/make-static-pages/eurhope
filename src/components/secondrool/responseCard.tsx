import Image from "next/image";
import { useState } from "react";

const ResponseCard = ({ response, t }: any) => {
  const [showMore, setShowMore] = useState(false);

  return (
    <div className=" w-full lg:w-auto card lg:card-side  flex shadow-xl rounded-md  max-w-[800px]  ">
      <div
        className={`flex items-center justify-center lg:rounded-l-md lg:rounded-tr-none  rounded-t-md lg:w-[100%] p-2 ${response.bgColor}`}
      >
        <Image
          alt="response"
          className="object-scale-down w-44 h-24"
          src={response.image}
          width={44}
          height={24}
          loading="lazy"
        />
      </div>
      <div className="flex-col p-5 lg:min-w-[640px]">
        <p className="font-circular_medium text-[16px]">
          {t.rich(response.text, {
            "link-party": (chunks: any) => (
              <a className="underline" target="_blank" href={response.link}>
                {chunks}
              </a>
            ),
          })}
          {/*showMore
            ? t.rich(response.text, {
                "link-party": (chunks: any) => (
                  <a className="underline" target="_blank" href={response.link}>
                    {chunks}
                  </a>
                ),
              })
            : `${t.rich(response.text, {
                "link-party": (chunks: any) => (
                  <a className="underline" target="_blank" href={response.link}>
                    {chunks}
                  </a>
                ),
              })}...`*/}
        </p>
        {t.rich(response.text, {
          "link-party": (chunks: any) => (
            <a className="underline" target="_blank" href={response.link}>
              {chunks}
            </a>
          ),
        }).length > 50 && (
          <span className="underline cursor-pointer text-gray-500" onClick={() => setShowMore(!showMore)}>
            {showMore ? "Less" : "More"}
          </span>
        )}
      </div>
    </div>
  );
};

export default ResponseCard;
