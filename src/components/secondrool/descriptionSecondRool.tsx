import Image from "next/image";
import eurHope from "@/assets/description.png";
import make from "@/assets/make.svg";
import yef from "@/assets/yef.svg";
import eur_parl from "@/assets/eur_parl.png";
import { useTranslations } from "next-intl";
const DescriptionSecondRool = () => {
  const t = useTranslations();

  return (
    <div className=" flex-col w-full  bg-white">
      <div className=" top-0 flex justify-center items-center  w-full h-[557px] bg-eurhope-100 ">
        <div className="flex-col justify-center items-center">
          <Image src={eurHope} width={694} height={390} alt="Picture of EurHope" />
          <div className="flex justify-center items-center space-x-10">
            <Image src={make} alt="make logo" />
            <Image src={yef} alt="yef logo" />
            <div className="flex-col justify-start space-y-1 pb-2">
              <p className="text-white text-[12px] font-light">Supported by </p>
              <Image src={eur_parl} width={141} height={51} className="rounded-md" alt="euro parl logo" />
            </div>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-center py-20">
        <div className="flex-col items-center justify-center  px-40 space-y-10">
          <p className="text-center font-bold text-[40px] leading-[55.6px] tracking-wide  ">
            {t("Explore the")}
            <span className="bg-eurhope-200 px-1 ml-2 ">{t("agenda of hope")}</span>
            <br />
            {t("european-political-parties")} <span className="bg-eurhope-200 px-1 ml-2">{t("responses")}</span>
          </p>
          <p className="  text-center font-[450px] leading-[25.3px] text-[20px] px-28">
            Welcome to EurHope, the first EU-wide, multilingual
            <strong> voting support tool.</strong> Based on the statistically representative priorities of young
            Europeans, you now can explore the responses by <strong> European political parties </strong> and civil
            society. You find them grouped along the seven dimensions and 15 consensual ideas of the
            <u> Agenda of Hope</u>, established by 1,5 Mio+ votes and 5000+ proposals.
          </p>
        </div>
      </div>
    </div>
  );
};

export default DescriptionSecondRool;
