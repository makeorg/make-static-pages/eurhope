import ResponseCard from "./responseCard";

const ResponsesSecondRool = ({ responses, t }: any) => {
  return (
    <div className=" w-full  flex-col justify-center items-center space-y-28 flex bg-white p-4 py-20   ">
      <div className="flex-row justify-center items-center space-y-10  ">
        <p className=" text-center font-circular_bold text-[42px]  leading-normal">
          <span className="bg-eurhope-200 mr-2 px-2">{t("second_response")}</span>
          {t("by_european_political_parties")}
        </p>

        <p className="font-circular_medium text-[18px] text-center text-black px-4 ">{t("second_description")}</p>
      </div>
      <div className="space-y-12">
        {responses
          ?.sort((a: any, b: any) => 0.5 - Math.random())
          .sort((a: any, b: any) => Number(b.hasResponse) - Number(a.hasResponse))
          .map((response: any, idx: number) => {
            return <ResponseCard response={response} key={idx} t={t} suppressHydrationWarning={true} />;
          })}
      </div>
    </div>
  );
};

export default ResponsesSecondRool;
