import footer from "@/assets/footer.png";
import Image from "next/image";

const Footer = () => {
  return (
    <div className="flex items-center justify-center mt-48">
      <Image src={footer} alt="footer EurHope" width={1920} />
    </div>
  );
};

export default Footer;
