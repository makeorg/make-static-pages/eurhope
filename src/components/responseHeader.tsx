import Breadcrumb from "@/components/Breadcrumb/index";

const ResponseHeader = ({ theme, idea, t, locale }: any) => {
  const breadCrumbItems: any = [
    {
      name: t("Homepage"),
      link: `/${locale}`,
      underline: true,
      icon: (
        <svg
          className="w-3 h-3 me-2.5"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="black"
          viewBox="0 0 20 20"
        >
          <path d="m19.707 9.293-2-2-7-7a1 1 0 0 0-1.414 0l-7 7-2 2a1 1 0 0 0 1.414 1.414L2 10.414V18a2 2 0 0 0 2 2h3a1 1 0 0 0 1-1v-4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v4a1 1 0 0 0 1 1h3a2 2 0 0 0 2-2v-7.586l.293.293a1 1 0 0 0 1.414-1.414Z" />
        </svg>
      ),
    },
    {
      name: `${t("Dimension")} - ${t(theme.title)}`,
      underline: true,
      link: `/${locale}/${theme.slug}`,
      icon: (
        <svg
          className="rtl:rotate-180 w-3 h-3 text-black mx-1"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 6 10"
        >
          <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="m1 9 4-4-4-4" />
        </svg>
      ),
    },
    {
      name: `${t("Idea")} - ${t(idea.description)}`,
      link: `/${locale}/${theme.slug}/${idea.slug}`,
      icon: (
        <svg
          className="rtl:rotate-180 w-3 h-3 text-black mx-1"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 6 10"
        >
          <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="m1 9 4-4-4-4" />
        </svg>
      ),
    },
  ];

  return (
    <div className={`w-full flex flex-col  justify-center items-center bg-clip-border  ${theme.bgColor} lg:h-[360px]`}>
      <div className="flex pt-4 lg:pl-36 lg:pt-10 w-full  ">
        <Breadcrumb items={breadCrumbItems} />
      </div>
      <div className="flex justify-center items-center py-20">
        <div className="flex  flex-col justify-center items-center space-y-5 h-auto  lg:h-[165px]  bg-white shadow-lg  rounded-md shadow-blue-gray-500/40 p-6">
          <div className="w-full  flex justify-center items-center">
            <p
              className={`text-black ${theme.bgColor} px-2 py-2 font-trade_gothic leading-[12px] text-[16px] uppercase`}
            >
              {t(theme.title)}
            </p>
          </div>
          <div className="w-full flex text-center justify-center items-center h-[60px] p-6 lg:px-32">
            <p className="text-[20px] text-center  font-circular_bold ">{t(idea.description)}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ResponseHeader;
