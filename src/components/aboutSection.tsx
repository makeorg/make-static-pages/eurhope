import Image from "next/image";
import about from "@/assets/about.jpg";
import Link from "next/link";

const AboutSection = ({ children, t, locale }: any) => {
  return (
    <div className="flex flex-col lg:flex-row justify-between w-full  bg-white mt-20">
      <div className=" lg:w-1/2 p-8  lg:p-24 flex flex-col  items-center lg:items-start space-y-8">
        <p className="font-circular_bold text-2xl text-black lg:text-3xl leading-normal lg:leading-snug tracking-wide">
          {t("About")}
          <span className="bg-eurhope-200 px-1 py-1  ml-1">{t("EurHope")}</span>
        </p>
        {children}
        <div className="flex flex-col space-y-4">
          <Link
            className=" w-full py-2 bg-eurhope-100 font-circular_black text-[17.04px] text-white  text-center uppercase rounded-full"
            href="https://about.make.org/why-this-consultation/eurhope"
          >
            {t("Learn more about Eurhope")}
          </Link>
          <Link
            target="_blank"
            className=" w-full py-2 px-2  bg-white font-circular_black text-[17.04px] text-eurhope-100 border border-eurhope-100 uppercase rounded-full"
            href={`https://elections.europa.eu/${locale}/`}
          >
            {t("More information about the European Elections")}
          </Link>
        </div>
      </div>

      <div className="overflow-hidden flex lg:items-end lg:justify-end justify-center bg-white">
        <Image
          src={about}
          alt={t("About") + t("EurHope")}
          width={650}
          height={543}
          className=" object-cover   h-full"
        />
      </div>
    </div>
  );
};

export default AboutSection;
