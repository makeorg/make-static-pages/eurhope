import Image from "next/image";
import Link from "next/link";

const ThemeCard = ({ theme, t, locale }: any) => {
  return (
    <div className={`${theme.bgColor} bg-opacity-[20%] w-full max-h-[350px] max-w-[360px] rounded shadow-md  p-[30px]`}>
      <div className="flex-row items-start space-y-5">
        <div className="flex-row items-start space-y-5">
          <Image
            width={400}
            src={theme.image}
            alt={t(theme.title)}
            className=" object-cover w-[300px] h-[150px]"
            loading="lazy"
          />
          <div className="w-full h-[25px] ">
            <span className={`text-black ${theme.bgColor} px-1 py-1 font-trade_gothic  text-[16px] uppercase`}>
              {t(theme.title)}
            </span>
          </div>
        </div>
        <div>
          <Link
            href={`/${locale}/${theme.slug}`}
            className="text-[#D5153C] font-circular_medium underline text-[16px] "
          >
            {t("Explore this dimension")}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ThemeCard;
