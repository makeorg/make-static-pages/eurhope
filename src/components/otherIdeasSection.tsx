import Link from "next/link";

const OtherIdeasSection = ({ otherThemeIdeas, t, locale }: any) => {
  return (
    <div className="w-full min-h-[628px] bg-black flex space-y-10  bg-opacity-5 flex-col justify-center items-center px-10 py-10">
      <div className="flex justify-center items-center text-center">
        <p className=" font-circular_bold text-[42px]">
          {t("Other ideas that might interest you!")}
        </p>
      </div>
      <div className="grid gap-x-8 gap-y-8 md:grid-cols-3 sm:grid-cols-2 lg:grid-cols-3">
        {otherThemeIdeas.map((themeIdea: any) => (
          <div
            key={`${themeIdea.themeSlug}-${themeIdea.slug}`}
            className={`bg-white  w-full min-h-[212px] max-w-[445px] rounded shadow-md  p-[30px]`}>
            <div className="flex-col items-start w-full space-y-10">
              <div className="flex-col items-start space-y-3">
                <span
                  className={`text-black ${themeIdea.themeBgColor} px-1 py-1 font-trade_gothic leading-[12px] text-[16px] uppercase`}>
                  {t(themeIdea.themeTitle)}
                </span>
                <p className="font-circular_medium">
                  {t(themeIdea.description)}
                </p>
              </div>

              <div className="flex-col justify-end ">
                <Link
                  href={`/${locale}/${themeIdea.themeSlug}/${themeIdea.slug}`}
                  className="text-[#D5153C] font-circular_medium underline text-[16px] ">
                  {t("Discover this idea")}
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
      <Link
        href={`/${locale}`}
        className=" min-w-[198px] min-h-[35px] px-4 py-2 flex items-center  bg-[#D5153C] font-circular_bold text-[16px]  text-center text-white  uppercase  rounded-[58px]  ">
        {t("Go back to the citizen agenda")}
      </Link>
    </div>
  );
};

export default OtherIdeasSection;
