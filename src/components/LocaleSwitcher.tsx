"use client";
import { useLocale, useTranslations } from "next-intl";
import { usePathname, useRouter, locales, localesName } from "../navigation";

export default function LocaleSwitcher() {
  const t = useTranslations();
  const locale = useLocale();
  const router = useRouter();
  const pathname = usePathname();

  return (
    <div className="inline-flex items-center h-full absolute right-5 top-0 lg:ml-0 justify-end">
      <div className="relative dropdown dropdown-bottom dropdown-end">
        <div
          tabIndex={0}
          role="button"
          className="btn bg-white  border-black border-1 m-1 hover:bg-white">
          <span>{`${locale.toUpperCase()}`}</span>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="m19.5 8.25-7.5 7.5-7.5-7.5"
            />
          </svg>
        </div>
        <div
          className="absolute bg-[#F9F9F9] right-0 w-80 mt-2 origin-top-right border border-gray-200   rounded-md shadow-lg outline-none p-5 dropdown-content z-[1] menu  grid grid-cols-2 gap-4"
          tabIndex={0}>
          {locales.map((locale) => (
            <button
              tabIndex={0}
              className="text-left text-black font-circular_medium "
              onClick={() => router.push(pathname, { locale })}
              key={locale}>{`${locale.toUpperCase()} - ${
              localesName[locale]
            }`}</button>
          ))}
        </div>
      </div>
    </div>
  );
}
