import Breadcrumb from "@/components/Breadcrumb/index";

const ThemeHeader = ({ theme, t, locale }: any) => {
  const breadCrumbItems: any = [
    {
      name: t("Homepage"),
      link: `/${locale}`,
      underline: true,
      icon: (
        <svg
          className="w-3 h-3 me-2.5"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="black"
          viewBox="0 0 20 20"
        >
          <path d="m19.707 9.293-2-2-7-7a1 1 0 0 0-1.414 0l-7 7-2 2a1 1 0 0 0 1.414 1.414L2 10.414V18a2 2 0 0 0 2 2h3a1 1 0 0 0 1-1v-4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v4a1 1 0 0 0 1 1h3a2 2 0 0 0 2-2v-7.586l.293.293a1 1 0 0 0 1.414-1.414Z" />
        </svg>
      ),
    },
    {
      name: `${t("Dimension")} - ${t(theme.title)}`,
      link: `/${locale}/${theme.slug}`,
      icon: (
        <svg
          className="rtl:rotate-180 w-3 h-3 text-black mx-1"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 6 10"
        >
          <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="m1 9 4-4-4-4" />
        </svg>
      ),
    },
  ];

  return (
    <div className="flex flex-col bg-clip-border w-full bg-white lg:h-[360px]  items-center">
      <div className={`flex pt-4 lg:pl-36 lg:pt-10 w-full h-[190px] ${theme.bgColor}`}>
        <Breadcrumb items={breadCrumbItems} />
      </div>
      <div className="relative flex justify-center items-center w-full lg:w-[800px] md:w-[400px] sm:[200px] h-[110px] overflow-hidden bottom-12 bg-white shadow-lg bg-clip-border rounded-[4px] shadow-blue-gray-500/40">
        <div className="h-[50px] flex justify-center  text-center items-center ">
          <span className={`text-black ${theme.bgColor} px-1 py-1 font-trade_gothic text-[32px] uppercase`}>
            {t(theme.title)}
          </span>
        </div>
      </div>
    </div>
  );
};

export default ThemeHeader;
